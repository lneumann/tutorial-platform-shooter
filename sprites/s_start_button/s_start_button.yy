{
    "id": "e5a2db54-d8ca-444c-914c-cc9ac06decc6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_start_button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "3bfc0047-59e1-4828-bb65-6591db17a18b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5a2db54-d8ca-444c-914c-cc9ac06decc6",
            "compositeImage": {
                "id": "b94c7308-4344-4353-8dfe-811f8c848a6e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3bfc0047-59e1-4828-bb65-6591db17a18b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97fcb774-072d-4798-a45b-595fd024fd01",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3bfc0047-59e1-4828-bb65-6591db17a18b",
                    "LayerId": "4dd51576-be93-42d9-84b1-8c62fafa2b10"
                }
            ]
        },
        {
            "id": "62603160-4f07-4875-8e63-3d311b5cf95a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5a2db54-d8ca-444c-914c-cc9ac06decc6",
            "compositeImage": {
                "id": "4de28eb7-2466-4b3d-b079-979b2451ef5a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62603160-4f07-4875-8e63-3d311b5cf95a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe7b557a-4f58-4b68-9512-eeb99bffc5ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62603160-4f07-4875-8e63-3d311b5cf95a",
                    "LayerId": "4dd51576-be93-42d9-84b1-8c62fafa2b10"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "4dd51576-be93-42d9-84b1-8c62fafa2b10",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e5a2db54-d8ca-444c-914c-cc9ac06decc6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}