{
    "id": "81b620f7-3b39-4c17-8874-f55400f9d46a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_enemy_bullet_hit",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 25,
    "bbox_left": 0,
    "bbox_right": 27,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "ea035921-e29a-442d-ae5c-2bcc9d2a4b58",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81b620f7-3b39-4c17-8874-f55400f9d46a",
            "compositeImage": {
                "id": "6653c486-aaa8-4b9e-9728-2a46b7185290",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea035921-e29a-442d-ae5c-2bcc9d2a4b58",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a6c67d6-abb6-4e46-8a13-5c62a5161b1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea035921-e29a-442d-ae5c-2bcc9d2a4b58",
                    "LayerId": "15d8ba44-6dc3-4248-afa9-b5a1c0056e5c"
                }
            ]
        },
        {
            "id": "e484b830-3ff2-4c0e-90c1-2a8eff3d54cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81b620f7-3b39-4c17-8874-f55400f9d46a",
            "compositeImage": {
                "id": "5b7ec30d-e625-4a8f-a76f-76ac59e01e4a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e484b830-3ff2-4c0e-90c1-2a8eff3d54cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "287049c2-d9f1-4e52-b81d-920bcf906bb7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e484b830-3ff2-4c0e-90c1-2a8eff3d54cc",
                    "LayerId": "15d8ba44-6dc3-4248-afa9-b5a1c0056e5c"
                }
            ]
        },
        {
            "id": "2bc794b5-0539-4dc7-9794-bdeb5371edbb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81b620f7-3b39-4c17-8874-f55400f9d46a",
            "compositeImage": {
                "id": "61cc18dd-1fbc-4d18-90f4-8256b779ad2e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2bc794b5-0539-4dc7-9794-bdeb5371edbb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a28a63d-d031-42e8-bf75-011051d6fd63",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2bc794b5-0539-4dc7-9794-bdeb5371edbb",
                    "LayerId": "15d8ba44-6dc3-4248-afa9-b5a1c0056e5c"
                }
            ]
        },
        {
            "id": "c1cfb542-8d8a-4838-950a-724e47afbd14",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81b620f7-3b39-4c17-8874-f55400f9d46a",
            "compositeImage": {
                "id": "48311385-ab85-495c-8872-5c00a793a3ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1cfb542-8d8a-4838-950a-724e47afbd14",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d3151f2-d849-4cfe-997b-7dbd28655927",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1cfb542-8d8a-4838-950a-724e47afbd14",
                    "LayerId": "15d8ba44-6dc3-4248-afa9-b5a1c0056e5c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 28,
    "layers": [
        {
            "id": "15d8ba44-6dc3-4248-afa9-b5a1c0056e5c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "81b620f7-3b39-4c17-8874-f55400f9d46a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 28,
    "xorig": 14,
    "yorig": 14
}