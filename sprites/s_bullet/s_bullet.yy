{
    "id": "77bc5be6-c60a-4a75-be7d-278688548320",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_bullet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 5,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "788c2740-1a8d-4a7a-9792-63dde719d7ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77bc5be6-c60a-4a75-be7d-278688548320",
            "compositeImage": {
                "id": "e61f1b46-b328-4210-92f0-97bd898f9038",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "788c2740-1a8d-4a7a-9792-63dde719d7ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76e168f0-5af9-4a47-b90b-71f46220434a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "788c2740-1a8d-4a7a-9792-63dde719d7ca",
                    "LayerId": "9fe05af3-3d68-4771-80b3-482e445cd47d"
                }
            ]
        },
        {
            "id": "e5c2641b-47f3-4d0e-8e55-317110affee2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77bc5be6-c60a-4a75-be7d-278688548320",
            "compositeImage": {
                "id": "f2fbf131-72e1-4843-8a31-e3d7401d4c33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5c2641b-47f3-4d0e-8e55-317110affee2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66897975-dad3-408a-a9d7-b1b91c495de4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5c2641b-47f3-4d0e-8e55-317110affee2",
                    "LayerId": "9fe05af3-3d68-4771-80b3-482e445cd47d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "9fe05af3-3d68-4771-80b3-482e445cd47d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "77bc5be6-c60a-4a75-be7d-278688548320",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 60,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 10,
    "yorig": 10
}