{
    "id": "8509153b-40dc-4845-822d-40ba824c13a9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_gun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "ca850fe9-5758-4f40-b858-69d4d49aed66",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8509153b-40dc-4845-822d-40ba824c13a9",
            "compositeImage": {
                "id": "11a2ad10-b85c-472d-9341-f475731422a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca850fe9-5758-4f40-b858-69d4d49aed66",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3bcbfe67-0068-4622-aee6-332b9d9acf14",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca850fe9-5758-4f40-b858-69d4d49aed66",
                    "LayerId": "ae5e8ed1-ca7d-4895-ba9d-9ef8df847fc5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 11,
    "layers": [
        {
            "id": "ae5e8ed1-ca7d-4895-ba9d-9ef8df847fc5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8509153b-40dc-4845-822d-40ba824c13a9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 3,
    "yorig": 5
}