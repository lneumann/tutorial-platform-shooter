{
    "id": "c7b3cac3-f4e4-42c5-9883-4ba65636dcf7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_enemy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 1,
    "bbox_right": 30,
    "bbox_top": 4,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "40f3a64f-90f3-4618-b65f-4bd322893e77",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c7b3cac3-f4e4-42c5-9883-4ba65636dcf7",
            "compositeImage": {
                "id": "c9fbe677-1c84-458e-a66b-db1067cc19cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40f3a64f-90f3-4618-b65f-4bd322893e77",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a694126c-22a9-4eb2-bb23-73da3d9792f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40f3a64f-90f3-4618-b65f-4bd322893e77",
                    "LayerId": "36b0c9d2-644f-43c3-8f1b-38b318899625"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "36b0c9d2-644f-43c3-8f1b-38b318899625",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c7b3cac3-f4e4-42c5-9883-4ba65636dcf7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}