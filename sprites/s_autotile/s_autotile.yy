{
    "id": "0454814a-ad38-4e82-92e8-f97c1b8e7509",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_autotile",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 8,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "e9d438cf-32cf-4bc5-82e9-5de23dfc6756",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0454814a-ad38-4e82-92e8-f97c1b8e7509",
            "compositeImage": {
                "id": "3cde8881-7dc8-46b9-9d52-2040b1039139",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9d438cf-32cf-4bc5-82e9-5de23dfc6756",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aaeeab27-6a32-403e-a2e4-fcfc1359978f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9d438cf-32cf-4bc5-82e9-5de23dfc6756",
                    "LayerId": "d7a7b918-e903-40c5-bd92-3ab5627ecdfd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d7a7b918-e903-40c5-bd92-3ab5627ecdfd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0454814a-ad38-4e82-92e8-f97c1b8e7509",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}