{
    "id": "bc560a18-ba97-4f71-ad11-8f095c5280ed",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_bullet_hit",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 25,
    "bbox_left": 0,
    "bbox_right": 27,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "ee887405-e1e8-477a-838d-b9c5f5ebf2a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bc560a18-ba97-4f71-ad11-8f095c5280ed",
            "compositeImage": {
                "id": "b750bcfc-1d59-4953-8f96-18f91bdef64a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee887405-e1e8-477a-838d-b9c5f5ebf2a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08b69998-e0b7-430d-8fc0-cbbf8f728860",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee887405-e1e8-477a-838d-b9c5f5ebf2a8",
                    "LayerId": "5ca90fd0-d91c-434a-af8b-64130885d288"
                }
            ]
        },
        {
            "id": "59f982ad-4893-4bce-b062-c7448a2c6a96",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bc560a18-ba97-4f71-ad11-8f095c5280ed",
            "compositeImage": {
                "id": "b7aa6be2-62d2-4aef-a0b0-015df9d7ad23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59f982ad-4893-4bce-b062-c7448a2c6a96",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e954d7b7-33c4-4302-bcff-50ad68868ae3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59f982ad-4893-4bce-b062-c7448a2c6a96",
                    "LayerId": "5ca90fd0-d91c-434a-af8b-64130885d288"
                }
            ]
        },
        {
            "id": "028c7a56-e417-4639-8bae-73dad65c6f28",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bc560a18-ba97-4f71-ad11-8f095c5280ed",
            "compositeImage": {
                "id": "9a3e0a94-75ff-4891-bb51-17c6b8cf1234",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "028c7a56-e417-4639-8bae-73dad65c6f28",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3cc26b99-7741-41a2-b0f8-987d65292fe9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "028c7a56-e417-4639-8bae-73dad65c6f28",
                    "LayerId": "5ca90fd0-d91c-434a-af8b-64130885d288"
                }
            ]
        },
        {
            "id": "e7d8a169-6183-449f-88f6-f709a208b890",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bc560a18-ba97-4f71-ad11-8f095c5280ed",
            "compositeImage": {
                "id": "2574314d-e7ed-4c55-8c6b-9984315e6fd7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7d8a169-6183-449f-88f6-f709a208b890",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "157f03a2-a7b9-4a50-8353-558a4a9dc688",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7d8a169-6183-449f-88f6-f709a208b890",
                    "LayerId": "5ca90fd0-d91c-434a-af8b-64130885d288"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 28,
    "layers": [
        {
            "id": "5ca90fd0-d91c-434a-af8b-64130885d288",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bc560a18-ba97-4f71-ad11-8f095c5280ed",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 28,
    "xorig": 14,
    "yorig": 14
}