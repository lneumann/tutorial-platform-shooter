{
    "id": "dc306eff-426a-400a-a51a-247cd2da5c13",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_enemy_bullet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 5,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "4d81c98b-c49c-4e75-b4d6-158b4f8eab12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dc306eff-426a-400a-a51a-247cd2da5c13",
            "compositeImage": {
                "id": "57903617-551b-45b4-9c91-82a782f183e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d81c98b-c49c-4e75-b4d6-158b4f8eab12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8929e0a8-71fd-4bf6-aefc-75fbd1cf1ed2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d81c98b-c49c-4e75-b4d6-158b4f8eab12",
                    "LayerId": "7bd0b676-6e27-4b3b-b4d0-f3ef0fad5a10"
                }
            ]
        },
        {
            "id": "6e118a38-8a54-49af-a676-8fe26f884b9b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dc306eff-426a-400a-a51a-247cd2da5c13",
            "compositeImage": {
                "id": "047d92bc-c38e-4ab3-8b4a-13a2c6e41bb4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e118a38-8a54-49af-a676-8fe26f884b9b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "edb3fca2-aa4a-4bb5-9b53-1af6ad6f2e55",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e118a38-8a54-49af-a676-8fe26f884b9b",
                    "LayerId": "7bd0b676-6e27-4b3b-b4d0-f3ef0fad5a10"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "7bd0b676-6e27-4b3b-b4d0-f3ef0fad5a10",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dc306eff-426a-400a-a51a-247cd2da5c13",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 60,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 10,
    "yorig": 10
}