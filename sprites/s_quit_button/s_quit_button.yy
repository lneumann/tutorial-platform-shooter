{
    "id": "24d388db-0388-43b4-95d4-59c303ae6c23",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_quit_button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "d43bb009-54cd-4e50-8daa-884ecbcad0b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24d388db-0388-43b4-95d4-59c303ae6c23",
            "compositeImage": {
                "id": "56d2afbc-bcd3-4a54-96ac-36aa6f9588cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d43bb009-54cd-4e50-8daa-884ecbcad0b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b07c2280-772d-4b31-a33b-1084e26e785a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d43bb009-54cd-4e50-8daa-884ecbcad0b7",
                    "LayerId": "d2f4c81b-956d-4d57-9c3e-82568cfe06f7"
                }
            ]
        },
        {
            "id": "3637391e-b946-4855-9c24-896ef647ee1f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24d388db-0388-43b4-95d4-59c303ae6c23",
            "compositeImage": {
                "id": "1afa8f38-3a50-48a1-a370-46d87fe34755",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3637391e-b946-4855-9c24-896ef647ee1f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ddb53dc8-e25c-4401-bde0-9eb7090448eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3637391e-b946-4855-9c24-896ef647ee1f",
                    "LayerId": "d2f4c81b-956d-4d57-9c3e-82568cfe06f7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "d2f4c81b-956d-4d57-9c3e-82568cfe06f7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "24d388db-0388-43b4-95d4-59c303ae6c23",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}