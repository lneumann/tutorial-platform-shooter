{
    "id": "817b5275-b52f-4f19-adeb-8982ad1172fe",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_player",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 36,
    "bbox_left": 3,
    "bbox_right": 26,
    "bbox_top": 4,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "65fa0ca7-163e-46a7-9cec-1b5dddba3e1b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "817b5275-b52f-4f19-adeb-8982ad1172fe",
            "compositeImage": {
                "id": "5c9b3d8d-23e6-460b-b6b1-a2546fcb9c34",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65fa0ca7-163e-46a7-9cec-1b5dddba3e1b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba23e40c-df9d-4c84-8eee-792dd7897952",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65fa0ca7-163e-46a7-9cec-1b5dddba3e1b",
                    "LayerId": "2bb36e09-8188-43a8-8bab-a610d097221d"
                }
            ]
        },
        {
            "id": "aed2a488-0735-435d-a3a8-c84e03bd25c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "817b5275-b52f-4f19-adeb-8982ad1172fe",
            "compositeImage": {
                "id": "0465390d-2b42-4b0b-ac70-a1fa483c0c10",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aed2a488-0735-435d-a3a8-c84e03bd25c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35f9091d-cbe4-4672-a2e9-d5137470b925",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aed2a488-0735-435d-a3a8-c84e03bd25c3",
                    "LayerId": "2bb36e09-8188-43a8-8bab-a610d097221d"
                }
            ]
        },
        {
            "id": "be92e61c-6ce3-467f-b921-5369a9d02d5d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "817b5275-b52f-4f19-adeb-8982ad1172fe",
            "compositeImage": {
                "id": "e466f425-8402-41a9-94cb-2fcc1037acf7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be92e61c-6ce3-467f-b921-5369a9d02d5d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66b051a1-bb47-4600-9751-4b57a3342118",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be92e61c-6ce3-467f-b921-5369a9d02d5d",
                    "LayerId": "2bb36e09-8188-43a8-8bab-a610d097221d"
                }
            ]
        },
        {
            "id": "6bfa56fe-aae1-46da-b830-ce9dd6eef59b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "817b5275-b52f-4f19-adeb-8982ad1172fe",
            "compositeImage": {
                "id": "8403b140-b3a8-4bf8-8847-52d30613eda9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6bfa56fe-aae1-46da-b830-ce9dd6eef59b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a80f4158-d72e-45c6-bed5-a5d8dda77e4a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6bfa56fe-aae1-46da-b830-ce9dd6eef59b",
                    "LayerId": "2bb36e09-8188-43a8-8bab-a610d097221d"
                }
            ]
        },
        {
            "id": "20a0e125-1a84-4740-b8e6-343b52f7e93d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "817b5275-b52f-4f19-adeb-8982ad1172fe",
            "compositeImage": {
                "id": "bffcd39a-5576-4e31-a626-15976ecbb30a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20a0e125-1a84-4740-b8e6-343b52f7e93d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77bb3153-7c06-4897-9d3e-d2706848cd32",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20a0e125-1a84-4740-b8e6-343b52f7e93d",
                    "LayerId": "2bb36e09-8188-43a8-8bab-a610d097221d"
                }
            ]
        },
        {
            "id": "51eb0713-7f86-4958-8a4e-8b0b80285f18",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "817b5275-b52f-4f19-adeb-8982ad1172fe",
            "compositeImage": {
                "id": "259974d3-926b-484c-9a2f-243823077cf8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51eb0713-7f86-4958-8a4e-8b0b80285f18",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b387264b-84ed-4778-a5bc-94133436686a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51eb0713-7f86-4958-8a4e-8b0b80285f18",
                    "LayerId": "2bb36e09-8188-43a8-8bab-a610d097221d"
                }
            ]
        },
        {
            "id": "6278baad-1e9b-48ec-b6d4-ef67d4c632e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "817b5275-b52f-4f19-adeb-8982ad1172fe",
            "compositeImage": {
                "id": "b721c2bf-a39a-477e-90f3-c282c08caaf9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6278baad-1e9b-48ec-b6d4-ef67d4c632e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c39afe5e-1a6a-44e5-9961-e692beea5f82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6278baad-1e9b-48ec-b6d4-ef67d4c632e5",
                    "LayerId": "2bb36e09-8188-43a8-8bab-a610d097221d"
                }
            ]
        },
        {
            "id": "7c33a82c-06e4-4ae3-a282-86262e1b7b3b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "817b5275-b52f-4f19-adeb-8982ad1172fe",
            "compositeImage": {
                "id": "abdeafa4-1f40-46bb-a7e9-72dd7da17175",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c33a82c-06e4-4ae3-a282-86262e1b7b3b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0dddfaaa-9990-4ed4-b3fc-cce3acaff716",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c33a82c-06e4-4ae3-a282-86262e1b7b3b",
                    "LayerId": "2bb36e09-8188-43a8-8bab-a610d097221d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 37,
    "layers": [
        {
            "id": "2bb36e09-8188-43a8-8bab-a610d097221d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "817b5275-b52f-4f19-adeb-8982ad1172fe",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 29,
    "xorig": 14,
    "yorig": 36
}