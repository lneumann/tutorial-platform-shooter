{
    "id": "d1120f45-bf38-4fbc-9b6f-0daa4d332e50",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_solid",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "5f4017d5-0eba-495c-8c39-cd5c127c94a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d1120f45-bf38-4fbc-9b6f-0daa4d332e50",
            "compositeImage": {
                "id": "ab7baef0-1c18-44ba-92e6-4d4b01b8eb6e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f4017d5-0eba-495c-8c39-cd5c127c94a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a9feb91-4874-47d8-8f24-4601632b63fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f4017d5-0eba-495c-8c39-cd5c127c94a8",
                    "LayerId": "60c99753-aaa1-444d-8968-dffad8df1b90"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "60c99753-aaa1-444d-8968-dffad8df1b90",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d1120f45-bf38-4fbc-9b6f-0daa4d332e50",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}