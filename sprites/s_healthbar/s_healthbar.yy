{
    "id": "9394e048-9cf3-4e9e-aa4c-66358074511e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_healthbar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "305759fe-3d82-4294-af8d-de84b570e1cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9394e048-9cf3-4e9e-aa4c-66358074511e",
            "compositeImage": {
                "id": "df30ca30-014d-49b9-b321-c20f44d4cf16",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "305759fe-3d82-4294-af8d-de84b570e1cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3079363d-ea34-44ff-8a37-b4edd73fe10e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "305759fe-3d82-4294-af8d-de84b570e1cb",
                    "LayerId": "1dc2d639-f882-4d03-9381-0d5472423e79"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "1dc2d639-f882-4d03-9381-0d5472423e79",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9394e048-9cf3-4e9e-aa4c-66358074511e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}