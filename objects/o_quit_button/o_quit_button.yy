{
    "id": "af153b8e-7346-4f1e-b823-820630451fde",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_quit_button",
    "eventList": [
        {
            "id": "e351c6c7-1728-47cc-8033-4e29e9ddf2d1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "af153b8e-7346-4f1e-b823-820630451fde"
        },
        {
            "id": "c39128db-ceed-4af0-8464-c874d5f5f647",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "af153b8e-7346-4f1e-b823-820630451fde"
        },
        {
            "id": "6cc1d7f2-9266-4a27-936f-b40162a4e2ed",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "af153b8e-7346-4f1e-b823-820630451fde"
        },
        {
            "id": "3291b6b4-1b98-4c05-bac1-77e53729123d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "af153b8e-7346-4f1e-b823-820630451fde"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "24d388db-0388-43b4-95d4-59c303ae6c23",
    "visible": true
}