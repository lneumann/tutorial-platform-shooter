// check health
if health_ <= 0 {
	instance_destroy();
}

// move
var hinput = keyboard_check(vk_right) - keyboard_check(vk_left);
// horizontal move
if hinput != 0 {
	// accelerate
	speed_[h] += hinput*acceleration_;
	speed_[h] = clamp(speed_[h], -max_hspeed_, max_hspeed_);
	image_speed = hinput*get_flipped*0.6;
} else {
	// deaccelerate
	speed_[h] = lerp(speed_[h], 0, 0.3);
	image_speed = 0;
	image_index = 0;
}

// gravity
if !place_meeting(x, y + 1, o_solid) {
	speed_[v] += gravity_;
	image_speed = 0;
	image_index = 6;
} else {
	// jump
	if keyboard_check_pressed(vk_up) {
		speed_[v] = jump_height_;
		
		// strech on jump
		x_scale_ = image_xscale*0.8;
		y_scale_ = image_yscale*1.2;
	}
}

move(speed_, 0);

// squash on landing
if place_meeting(x, y + 1, o_solid) && !place_meeting(xprevious, yprevious + 1, o_solid) {
	x_scale_ = image_xscale*1.2;
	y_scale_ = image_yscale*0.8;
}

// reset strech
x_scale_ = lerp(x_scale_, image_xscale, 0.2);
y_scale_ = lerp(y_scale_, image_yscale, 0.2);

// warp
warp();