
var flipped = get_flipped();
var gun_x = x-4*flipped;
var gun_y = y-sprite_height/2;
var rot = point_direction(x, gun_y, mouse_x, mouse_y);

if interval_is_off(alarm[1], 16) {
	gpu_set_fog(false, c_white, 0, 1);
} else {
	gpu_set_fog(true, c_white, 0, 1);
}

// draw the player
// ceil(y) - pixel accurate landing
draw_sprite_ext(s_player, image_index, x, ceil(y), x_scale_*flipped, y_scale_, 0, image_blend, image_alpha);

// draw the gun
draw_sprite_ext(s_gun, 0, gun_x, gun_y, 1, flipped, rot, image_blend, image_alpha);

gpu_set_fog(false, c_white, 0, 1);