{
    "id": "7cc57b8e-07a3-402f-8a31-4ae04e8b5f1e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_player",
    "eventList": [
        {
            "id": "02883536-5772-4483-9a9c-bed3ad66d5c3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7cc57b8e-07a3-402f-8a31-4ae04e8b5f1e"
        },
        {
            "id": "56eb514c-128b-4083-a6f4-718bd111162e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "7cc57b8e-07a3-402f-8a31-4ae04e8b5f1e"
        },
        {
            "id": "2c75ecda-737e-4950-82cc-e3467d4b1b30",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "7cc57b8e-07a3-402f-8a31-4ae04e8b5f1e"
        },
        {
            "id": "70a60265-c21a-4c74-9b86-25743e2cd1c4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 50,
            "eventtype": 6,
            "m_owner": "7cc57b8e-07a3-402f-8a31-4ae04e8b5f1e"
        },
        {
            "id": "a636a44c-09ec-4607-a817-03fcc6c33e50",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "7cc57b8e-07a3-402f-8a31-4ae04e8b5f1e"
        },
        {
            "id": "0714d37e-9ef8-407f-b1ad-8ef9291ad988",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "7aa3579d-b5db-4365-824f-aef1961da9b3",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "7cc57b8e-07a3-402f-8a31-4ae04e8b5f1e"
        },
        {
            "id": "e6bfe8a0-0cab-4723-8ecd-aec24773830e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "7cc57b8e-07a3-402f-8a31-4ae04e8b5f1e"
        },
        {
            "id": "97d5bf6d-c4e9-448f-924c-5501e1b986fc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "7cc57b8e-07a3-402f-8a31-4ae04e8b5f1e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "817b5275-b52f-4f19-adeb-8982ad1172fe",
    "visible": true
}