// variables
speed_ = [0, 0];
max_hspeed_ = 4;
vspeed_ = 0;
gravity_ = 0.5;
acceleration_ = 1;
frication = 0.3;
jump_height_ = -10;
max_health_ = 5;
health_ = max_health_;
invincible_ = false;

// map keys
keyboard_set_map(ord("W"), vk_up);
keyboard_set_map(ord("A"), vk_left);
keyboard_set_map(ord("S"), vk_down);
keyboard_set_map(ord("D"), vk_right);

// bullet cooldown
bullet_cooldown_ = room_speed / 4;
alarm[0] = bullet_cooldown_;

// strech scale
x_scale_ = image_xscale;
y_scale_ = image_yscale;

// hide solids layer
var solids_layer = layer_get_id("Solids");
layer_set_visible(solids_layer, debug_mode);