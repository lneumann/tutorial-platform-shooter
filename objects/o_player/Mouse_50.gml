// create bullet
if alarm[0] <= 0 {
	var flipped = get_flipped();
	var gun_x = x - 4*flipped;
	var gun_y = y - sprite_height/2;
	var dir = point_direction(x, gun_y , mouse_x, mouse_y);
	var x_offset = lengthdir_x(16, dir);
	var y_offset = lengthdir_y(16, dir);

	var bullet = instance_create_layer(gun_x + x_offset, gun_y + y_offset, "Instances", o_bullet);
	bullet.image_angle = dir;
	bullet.direction = dir;
	alarm[0] = bullet_cooldown_;
}