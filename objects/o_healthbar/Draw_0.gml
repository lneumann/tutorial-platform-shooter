draw_self();

if !instance_exists(o_player) exit;

health_draw_ = lerp(health_draw_, o_player.health_, 0.3);
var health_perc = health_draw_ / o_player.max_health_;

draw_set_color(c_red);
draw_rectangle(x + 4, y + 4, x + 123 * health_perc, y + 11, false)
draw_set_color(c_white);


