{
    "id": "8fa3fe69-752a-44a0-b796-13f07b168a6b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_healthbar",
    "eventList": [
        {
            "id": "63ba426c-324c-4467-b881-2746f22ff06f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8fa3fe69-752a-44a0-b796-13f07b168a6b"
        },
        {
            "id": "3499d3e2-5fae-4a25-bcd6-5eac3461668a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "8fa3fe69-752a-44a0-b796-13f07b168a6b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "9394e048-9cf3-4e9e-aa4c-66358074511e",
    "visible": true
}