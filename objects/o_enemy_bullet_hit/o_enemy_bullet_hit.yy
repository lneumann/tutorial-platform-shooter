{
    "id": "b75a2cbd-0cfc-4cfc-8249-7d5c4d7430fa",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_enemy_bullet_hit",
    "eventList": [
        {
            "id": "deb9a16f-dbd8-4644-8744-c418275b5b9f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "b75a2cbd-0cfc-4cfc-8249-7d5c4d7430fa"
        },
        {
            "id": "56ab1044-6943-493d-a469-31e42c50848c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b75a2cbd-0cfc-4cfc-8249-7d5c4d7430fa"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "81b620f7-3b39-4c17-8874-f55400f9d46a",
    "visible": true
}