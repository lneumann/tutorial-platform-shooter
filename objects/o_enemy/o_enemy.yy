{
    "id": "162ceec0-1cc1-43a8-854c-4fb24157fa66",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_enemy",
    "eventList": [
        {
            "id": "3cd5239d-3a46-4f91-8e7d-fa0c12133440",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "162ceec0-1cc1-43a8-854c-4fb24157fa66"
        },
        {
            "id": "242ca71e-f733-424c-8de0-f6b44c91a4e9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "162ceec0-1cc1-43a8-854c-4fb24157fa66"
        },
        {
            "id": "efa3f4b2-80be-4c54-81db-c4d2891a9a71",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "162ceec0-1cc1-43a8-854c-4fb24157fa66",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "162ceec0-1cc1-43a8-854c-4fb24157fa66"
        },
        {
            "id": "be18d405-35e5-45a9-8613-4db0cd63504f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "57106452-c876-4b48-8611-741378b6184e",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "162ceec0-1cc1-43a8-854c-4fb24157fa66"
        },
        {
            "id": "fe43e8b4-12bc-4ca6-9b50-2733e41effc5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "162ceec0-1cc1-43a8-854c-4fb24157fa66"
        },
        {
            "id": "e6fe0b47-ddbd-442e-93ed-3d51d2dad212",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "162ceec0-1cc1-43a8-854c-4fb24157fa66"
        },
        {
            "id": "fcc4f5b8-33ef-438c-8065-006ccc392e0f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "162ceec0-1cc1-43a8-854c-4fb24157fa66"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "c7b3cac3-f4e4-42c5-9883-4ba65636dcf7",
    "visible": true
}