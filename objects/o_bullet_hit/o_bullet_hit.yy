{
    "id": "7ad53298-ea2d-4b2a-84bb-eb6fcd7daefc",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_bullet_hit",
    "eventList": [
        {
            "id": "958ad544-053e-4eb4-b4c0-1b3fba07cfa4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "7ad53298-ea2d-4b2a-84bb-eb6fcd7daefc"
        },
        {
            "id": "bb7bc981-371c-477e-99da-9e43f7f6e4ba",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7ad53298-ea2d-4b2a-84bb-eb6fcd7daefc"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "bc560a18-ba97-4f71-ad11-8f095c5280ed",
    "visible": true
}