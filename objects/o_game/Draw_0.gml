if sprite_exists(pause_sprite_) && paused_ {
	draw_sprite_ext(pause_sprite_, 0, 0, 0, 0.5, 0.5, 0, c_gray, 1);
	draw_set_halign(fa_center);
	draw_set_valign(fa_middle);
	draw_text(display_get_gui_width()/4, display_get_gui_height()/4, "Paused")
}