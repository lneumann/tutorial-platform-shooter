if not paused_ {
	paused_ = true;
	if sprite_exists(pause_sprite_) sprite_delete(pause_sprite_)
	pause_sprite_ = sprite_create_from_surface(application_surface, 0, 0, display_get_gui_width(), display_get_gui_height(), false, false, 0, 0)
	instance_deactivate_all(true);
} else {
	paused_ = false;
	instance_activate_all();
}