{
    "id": "7aa3579d-b5db-4365-824f-aef1961da9b3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_enemy_bullet",
    "eventList": [
        {
            "id": "ecb619fe-6678-4d71-afbf-a867ecf54399",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7aa3579d-b5db-4365-824f-aef1961da9b3"
        },
        {
            "id": "4df9433b-f1f1-4d7c-a5b6-1057c8032018",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "2fbc111e-2f99-45ac-b875-794a6fb34bb0",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "7aa3579d-b5db-4365-824f-aef1961da9b3"
        },
        {
            "id": "18a24e35-eb82-4d70-9f60-ae92b3882a96",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "7aa3579d-b5db-4365-824f-aef1961da9b3"
        },
        {
            "id": "bf11f557-61fe-440e-93a8-930e0191c03c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "7aa3579d-b5db-4365-824f-aef1961da9b3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "dc306eff-426a-400a-a51a-247cd2da5c13",
    "visible": true
}