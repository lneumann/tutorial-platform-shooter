{
    "id": "57106452-c876-4b48-8611-741378b6184e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_bullet",
    "eventList": [
        {
            "id": "713aba4b-669b-491a-af65-67e12778fe59",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "57106452-c876-4b48-8611-741378b6184e"
        },
        {
            "id": "4bf4fe61-dcd4-48be-b8c6-8513cc41f9d6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "2fbc111e-2f99-45ac-b875-794a6fb34bb0",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "57106452-c876-4b48-8611-741378b6184e"
        },
        {
            "id": "36e1a779-68d2-41de-8474-583b63c7f361",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "57106452-c876-4b48-8611-741378b6184e"
        },
        {
            "id": "e16bb779-d3ae-426c-b456-55a9d75d9cf1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "57106452-c876-4b48-8611-741378b6184e"
        },
        {
            "id": "c65117e0-8d74-4683-9623-70ff555996d1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "57106452-c876-4b48-8611-741378b6184e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "77bc5be6-c60a-4a75-be7d-278688548320",
    "visible": true
}