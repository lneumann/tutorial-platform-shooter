{
    "id": "62749884-ba44-4b94-8470-248e9737ba54",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_start_button",
    "eventList": [
        {
            "id": "5d0023c3-265f-4254-96db-1d28c2e5b523",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "62749884-ba44-4b94-8470-248e9737ba54"
        },
        {
            "id": "eed773be-fdce-4e04-afb1-879bf98313fc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "62749884-ba44-4b94-8470-248e9737ba54"
        },
        {
            "id": "1f826d5b-e4f7-4a76-bca8-9539bc494244",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "62749884-ba44-4b94-8470-248e9737ba54"
        },
        {
            "id": "7cffba18-8680-48fa-8c16-127c9962b8fc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "62749884-ba44-4b94-8470-248e9737ba54"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "e5a2db54-d8ca-444c-914c-cc9ac06decc6",
    "visible": true
}